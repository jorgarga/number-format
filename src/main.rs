fn main() {
    for number in 0..i64::MAX {
        println!(
            "{number:064b} - {number:08X} - {number:20}",
            number = number
        );
    }
}
