# Number format
This little program counts from 0 to ```64::MAX``` ¡That's a lot!
(The actual number for  ```64::MAX```  is 9.223.372.036.854.775.807)

As an excuse to practice [formatted print](https://doc.rust-lang.org/rust-by-example/hello/print.html) the program shows a number representation in:
*  binary
* hexadecimal
* decimal
 
